package com.yaremchuk.controller;

import com.yaremchuk.model.Model;

public class Controller {

    Model model = new Model();

    public String pingPong() {
        return model.pingPong();
    }

    public void simpleFibonacci() {
        model.fibonacci();
    }

    public void executorsFibonacci() {
        model.executorsWithFibonacci();
    }

    public void testScheduledThreadPool(int quantity) {
        try {
            model.testScheduledThreadPool(quantity);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testSynchronized() {
        Thread main = Thread.currentThread();
        try {
            main.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        model.testSynchronizedOnObject();
        main.notify();
    }

}

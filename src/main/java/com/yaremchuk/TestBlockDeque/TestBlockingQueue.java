package com.yaremchuk.TestBlockDeque;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class TestBlockingQueue {

    BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(5);

    public void testQueue() {
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    queue.put(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Put: " + i);
            }
        });

        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    System.out.println("Take: " + queue.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread1.start();
        thread2.start();
    }

    public static void main(String[] args) {
        TestBlockingQueue tbq = new TestBlockingQueue();
        tbq.testQueue();
    }
}

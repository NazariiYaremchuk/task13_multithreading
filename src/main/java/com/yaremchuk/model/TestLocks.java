package com.yaremchuk.model;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class TestLocks {
    private static ReadWriteLock rw = new ReentrantReadWriteLock();
    private static Lock readLock = rw.readLock();
    private static Lock writeLock = rw.writeLock();
    private Monitor monitor = new Monitor();

    public void firstMethod() {
        new Thread(() -> {
            readLock.lock();
            System.out.print("\n First function: ");
            monitor.getArray().stream().forEach(el -> {
                System.out.print(el + " ");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            });
            readLock.unlock();
        }).start();
    }

    public void secondMethod() {
        new Thread(() -> {
            writeLock.lock();
            System.out.print("\n Second function: ");
            for (int i = 0; i < 7; i++)
                monitor.addToArray((Integer.valueOf((int) (Math.random() * 20))));
            writeLock.unlock();
        }).start();

    }

    public void thirdMethod() {
        new Thread(() -> {
            readLock.lock();
            System.out.print("\n Third function: ");
            monitor.getArray().stream().forEach(el -> {
                System.out.print(el + " ");

            });
            readLock.unlock();
        }).start();
    }
}

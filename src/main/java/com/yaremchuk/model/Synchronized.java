package com.yaremchuk.model;

/**
 * Create a class with three methods containing critical sections that all synchronize on the same object.
 * Create multiple tasks to demonstrate that only one of these methods can run at a time.
 * Now modify the methods so that each one synchronizes on a different object and show that all three methods can be running at once.
 */
public class Synchronized {

    private static Monitor monitor = new Monitor();

    public void firstMethod() {
        new Thread(() -> {
            synchronized (monitor) {
                System.out.print("\n First function: ");
                monitor.getArray().stream().forEach(el->{
                    System.out.print(el+" ");
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });

            }
        }).start();
    }

    public void secondMethod() {
        new Thread(() -> {
            synchronized (monitor) {
                System.out.print("\n Second function: ");
                monitor.getArray().stream().forEach(el->{
                    System.out.print(el+" ");
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        }).start();

    }

    public void thirdMethod() {
        new Thread(() -> {
            synchronized (monitor) {
                System.out.print("\n Third function: ");
                monitor.getArray().stream().forEach(el->{
                    System.out.print(el+" ");
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        }).start();
    }
}
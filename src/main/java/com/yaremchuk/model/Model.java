package com.yaremchuk.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Model {

    /**
     * Write simple “ping-pong” program using wait() and notify().
     */
    private volatile static long count = 0;
    private static Object monitor = new Object();

    public String pingPong() {
        StringBuilder result = new StringBuilder(" ");
        Thread t1 = new Thread(() -> {
            synchronized (monitor) {
                for (int i = 0; i < 10000000; i++) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    count++;
                    monitor.notify();
                }

                result.append("Finish: " + Thread.currentThread().getName() + " " + LocalDateTime.now());
            }

        });

        Thread t2 = new Thread(() -> {
            synchronized (monitor) {
                for (int i = 0; i < 10000000; i++) {
                    monitor.notify();
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    count++;
                }
                result.append("Finish: " + Thread.currentThread().getName() + " " + LocalDateTime.now()
                );
            }
        });

        result.append(LocalDateTime.now()
        );
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return result.toString() + " Count = " + count;
    }

    /**
     * Create a task that produces a sequence of n Fibonacci numbers, where n is provided
     * to the constructor of the task. Create a number of these tasks and drive them using
     * threads.
     */
    public void fibonacci() {
        for (int i = 15; i < 20; i++) {
            Thread t = new Thread(new FibonacciTask(i));
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            t.start();
        }
        return;
    }

    /**
     * Repeat previous exercise using the different types of executors.
     */
    public void executorsWithFibonacci() {
        ExecutorService pool = Executors.newCachedThreadPool();
        for (int i = 20; i < 25; i++) {
            pool.execute(new FibonacciTask(i));
        }
        pool.shutdown();

        // FixedThreadPool
        pool = Executors.newFixedThreadPool(5);
        for (int i = 25; i < 30; i++) {
            pool.execute(new FibonacciTask(i));
        }
        pool.shutdown();

        // SingleThreadExecutor
        pool = Executors.newSingleThreadExecutor();
        for (int i = 35; i < 40; i++) {
            pool.execute(new FibonacciTask(i));
        }
        pool.shutdown();
    }

    /**
     * Modify Exercise 2 so that the task is a Callable that sums the values of all the Fibonacci
     * numbers. Create several tasks and display the results.
     */

    public void fibonacciCallable() {
        ThreadMethod tm = new ThreadMethod();
        List<Future<Integer>> futures = new ArrayList<>();
        for (int i = 15; i < 20; i++) {
            futures.add(tm.runTask(i));
        }

        for (Future<Integer> f : futures) {
            try {
                System.out.println(f.get() + " ");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            } finally {
                tm.shutdown();
            }
        }
    }

    /**
     * Create a task that sleeps for a random amount of time between 1 and 10 seconds,
     * then displays its sleep time and exits. Create and run a quantity (given on the command
     * line) of these tasks. Do it by using ScheduledThreadPool.
     */
    public void testScheduledThreadPool(int quantity) throws InterruptedException {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(quantity);

        for (int i = 0; i < quantity; i++) {
            int sleepingTime = (int) ((Math.random() * 10) + 1);
            Thread thread = new Thread(() -> {
                System.out.println("Sleeping time of thread: " + sleepingTime);
            });

            scheduledThreadPool.schedule(thread, sleepingTime, TimeUnit.SECONDS);
        }
        scheduledThreadPool.shutdown();
    }

    public void testSynchronizedOnObject() {
        Synchronized object = new Synchronized();
        object.firstMethod();
        object.secondMethod();
        object.thirdMethod();
    }
}

class ThreadMethod {
    private ExecutorService pool = Executors.newCachedThreadPool();

    Future<Integer> runTask(int count) {
        return pool.submit(new FibonacciCallable(count));
    }

    void shutdown() {
        pool.shutdown();
    }
};
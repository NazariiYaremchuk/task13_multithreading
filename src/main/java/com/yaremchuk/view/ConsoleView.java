package com.yaremchuk.view;

import com.yaremchuk.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ConsoleView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Test Ping Pong");
        menu.put("2", "2 - Simple Fibonacci driving");
        menu.put("3", "3 - Fibonacci driving using executors");
        menu.put("4", "4 - Test ScheduledThreadsPool");
        menu.put("5", "5 - Test Synchronized on same object");
        menu.put("Q", "Q");
    }

    public ConsoleView() {

        controller = new Controller();
        input = new Scanner(System.in);

        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pingPong);
        methodsMenu.put("2", this::simpleFibonacci);
        methodsMenu.put("3", this::fibonacciAndExecutors);
        methodsMenu.put("4", this::testSheduledThreadsPool);
        methodsMenu.put("5", this::testSynchronized);

    }

    private void testSynchronized() {
//        try {
//            Thread.sleep(4000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        controller.testSynchronized();
    }

    private void pingPong() {
        logger.info(controller.pingPong());
    }

    private void simpleFibonacci() {
        controller.simpleFibonacci();
    }

    private void fibonacciAndExecutors() {
        controller.executorsFibonacci();
    }

    private void testSheduledThreadsPool() {
        logger.info("Enter number of threads: ");
        int quantity = 0;
        try {
            quantity = input.nextInt();
        } catch (NumberFormatException e) {
            logger.error(e.getMessage());
        }
        controller.testScheduledThreadPool(quantity);
    }


    //-------------------------------------------------------------------------
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
